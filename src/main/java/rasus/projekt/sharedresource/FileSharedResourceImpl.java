package rasus.projekt.sharedresource;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileSharedResourceImpl implements SharedResource {

    private static Path path;
    private static List<String> fileAllLines;
    private static String USED_SUFFIX = "#";

    public FileSharedResourceImpl(String path) {
        try {
            this.path = Paths.get(path);
            fileAllLines = Files.readAllLines(this.path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getItem(String userName) {
        String line;
        for(int i = 0, numOfLines = fileAllLines.size(); i<numOfLines; i++){
            line = fileAllLines.get(i);
            if(!line.contains(USED_SUFFIX)){
                fileAllLines.set(i, line + USED_SUFFIX + userName);
                try {
                    Files.write(path, fileAllLines, Charset.forName("UTF-8"));
                    return line;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return null;
    }

    @Override
    public void synchronize() {
        try {
            fileAllLines = Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
