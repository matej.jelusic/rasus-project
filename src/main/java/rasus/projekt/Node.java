package rasus.projekt;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import rasus.projekt.sharedresource.FileSharedResourceImpl;
import rasus.projekt.sharedresource.SharedResource;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeoutException;

public class Node {
    private final static String QUEUE_NAME = "queueName3";
    private final static String SHARED_FILE = "src/main/resources/sharedResource.txt";
    private static String myUsername = "";
    private static String token = "custom token";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        myUsername = getUserName();
        SharedResource fileSharedResource = new FileSharedResourceImpl(SHARED_FILE);
        CustomConsumer customConsumer = new CustomConsumer(channel, fileSharedResource, myUsername, token, QUEUE_NAME);

        boolean autoAck = false;
        channel.basicConsume(QUEUE_NAME, autoAck, customConsumer);

        if(hasInitialToken(args)){
            channel.basicPublish("", QUEUE_NAME, null, token.getBytes());
        }

        System.out.println("Waiting for token...");
    }

    private static boolean hasInitialToken(String[] args) {
        if(args.length<1){
            return false;
        }
        else return true;
    }

    public static String getUserName() {
        Random rand = new Random();

        int  n = rand.nextInt(999) + 1;
        String name = "Node-" + n;
        System.out.println("Name: " + name);
        return "Node-" + n;
    }
}
